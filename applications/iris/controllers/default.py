# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------
import logging
logger = logging.getLogger("web2py.app.iris")
logger.setLevel(logging.DEBUG)
import sys




def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))

@auth.requires_login()
def ask():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    logger.debug("outside")
    
    if(request.vars.fromdate):
        try:
            logger.debug("before")
            
            ret = db.listings.insert(byid=auth.user.id,product=request.vars.product,fromloc=request.vars.fromloc,toloc=request.vars.toloc,fromdate=request.vars.fromdate,todate=(request.vars.todate),completed=False,acceptedcounter=0,started=0)
            logger.debug(ret)
            response.flash = T(str("create"))
            
            
        except Exception as e:
            logger.debug(str(e))
            pass
            
            
            
        
    
    return dict(message=T('Welcome to web2py!'))


@auth.requires_login()
def contact():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    
    return dict(message=T('Welcome to web2py'))
        
    
@auth.requires_login()
def saverequest():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    try:
        ret  = db.request.insert(listingid=request.args(0),requesterid=request.args(1))
        return "request sent"
    
    except:
        return "sending request failed"
    
@auth.requires_login()
def acceptrequest():
    try:
        ret = db(db.listings.id == request.args(0)).update(acceptedcounter = '1')
        ret = db(db.request.listingid == request.args(0)).update(acceptedcounter = '1')
        return "request accepted"
    except:
        return "sending accept failed"   


@auth.requires_login()
def finishrequest():
    try:
        logger.debug(request.args(0))
        logger.debug(str(db(db.listings.id == request.args(0)).select(db.listings.completed)))
        ret = db(db.listings.id == request.args(0)).update(completed=True)
        logger.debug(str(ret))
        logger.debug("after finishing")
        return "request finished"
    except:
        return "finishing request failed"
    

    
@auth.requires_login()
def offer():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    #offers = db().select(db.listings.fromloc, db.listings.toloc, db.listings.fromdate, db.listings.todate, db.listings.product)
    offers = SQLFORM.grid((db.listings.acceptedcounter < 1), fields={db.listings.id, db.listings.product, db.listings.byid, db.listings.fromloc, db.listings.toloc, db.listings.fromdate, db.listings.todate, db.listings.acceptedcounter}, editable=False, deletable=False, csv=False, user_signature=False, paginate=25, create=False, links=[dict(header="request", body=lambda row:fnview(row))])
    
    try:
        pass
    except:
        pass
            
    return dict(offers=offers)


@auth.requires_login()
def startdelivering():
    try:
        logger.debug("before start delivering")
        ret = db(db.listings.id == request.args(0)).update(started=1)
    except Exception as ex:
        return str(ex)

@auth.requires_login()
def startdelivery():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    logger.debug("inside")
    listingids = db((db.request.requesterid ==  auth.user.id) & (db.request.acceptedcounter ==  1)).select(db.request.listingid)
    logger.debug(str(listingids))
    listlistingids = []
    
 
    for row in listingids:
        logger.debug(row.listingid)
        listlistingids.append(row.listingid)
    
    logger.debug(str(listlistingids))
    
    
    
    
    #qryRequests = db(db.request.listingid.belongs(listlistingids)).select(db.request.listingid, db.request.requesterid)
    tostart = SQLFORM.grid(db.listings.id.belongs(listlistingids), editable=False, deletable=False, csv=False, user_signature=False, paginate=25, create=False, links=[dict(header="start", body=lambda row:fnstart(row))])
    

            
    return dict(tostart=tostart)


@auth.requires_login()
def fnstart(row):
    return INPUT(_type='button',  _value='start', _onclick="javascript:startdelivery('" + str(row.id) + "')")
    
    


@auth.requires_login()
def requests():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    
    listingids = db((db.listings.byid ==  auth.user.id)).select(db.listings.id, db.listings.acceptedcounter, db.listings.started)
    
    listlistingids = []
    
    maplistingsaccept = {}
    maplistingsstarted = {} 
    
    for row in listingids:
        
        listlistingids.append(row.id)
        maplistingsaccept[str(row.id)] = str(row.acceptedcounter)
        maplistingsstarted[str(row.id)] = str(row.started)

    qryRequests = db(db.request.listingid.belongs(listlistingids)).select(db.request.listingid, db.request.requesterid)
    requests = SQLFORM.grid(db.request.listingid.belongs(listlistingids), editable=False, deletable=False, csv=False, user_signature=False, paginate=25, create=False, links=[dict(header="accept", body=lambda row:fnaccept(row,maplistingsaccept)), dict(header="finish", body=lambda row:fnfinish(row,maplistingsstarted))])
    
    try:
        pass
    except:
        pass
            
    return dict(requests=requests)



def fnfinish(row, maplistingstarted):
    logger.debug(str(row))
    if(maplistingstarted[row.listingid] == 0):
        return  INPUT(_type='button', _disabled='true', _value='finish')
    else:
        return  INPUT(_type='button', _value='finish', _onclick="javascript:finishrequest('" + str(row.listingid) + "')")

def fnview(row):
    if row.acceptedcounter == 1:
        return  INPUT(_type='button', _disabled='true', _value='request', _onclick="javascript:$(this).attr('disabled','disabled');saverequest('" + str(row.id) + "','"+str(auth.user.id)+"')")
    else:
        return  INPUT(_type='button',  _value='request', _onclick="javascript:$(this).attr('disabled','disabled');saverequest('" + str(row.id) + "','"+str(auth.user.id)+"')")


def fnaccept(row, maplistingsaccept):
    
    logger.debug(str(row))
    logger.debug(str(maplistingsaccept))
    logger.debug(maplistingsaccept[row.listingid] == '1')
    
    if(maplistingsaccept[row.listingid] == '1'):
        return  INPUT(_type='button', _disabled='true', _value='accept', _onclick="javascript:acceptrequest('" + str(row.listingid) +"')")
    else:
        return  INPUT(_type='button', _value='accept', _onclick="javascript:acceptrequest('" + str(row.listingid) +"')")


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


